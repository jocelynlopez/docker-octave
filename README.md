# Docker-octave

## Description
GNU Octave is a high-level language, primarily intended for numerical computations.
It provides a convenient command line interface for solving linear and nonlinear problems numerically, and for performing other numerical experiments using a language that is mostly compatible with Matlab. It may also be used as a batch-oriented language.

Octave has extensive tools for solving common numerical linear algebra problems, finding the roots of nonlinear equations, integrating ordinary functions, manipulating polynomials, and integrating ordinary differential and differential-algebraic equations. It is easily extensible and customizable via user-defined functions written in Octave’s own language, or using dynamically loaded modules written in C++, C, Fortran, or other languages.


Note: This container use the healthcheck command to ensure that is running correctly.

## Build
```bash
docker build -t jocelynlopez/octave .
```

## Run
```bash
docker run --rm jocelynlopez/octave --help
```

## Contributing
Please submit and comment on bug reports and feature requests.

To submit a patch:

1. Fork it (https://bitbucket.org/jocelynlopez/docker-octave/fork).
2. Create your feature branch (*git checkout -b my-new-feature*).
3. Make changes.
4. Commit your changes (*git commit -am 'Add some feature'*).
5. Push to the branch (*git push origin my-new-feature*).
6. Create a new Pull Request.


## License
This app is licensed under the MIT license.

## Warranty
This software is provided "as is" and without any express or
implied warranties, including, without limitation, the implied
warranties of merchantibility and fitness for a particular
purpose.
